from collections import UserDict
import json
import requests
import logging
import sys
from functools import lru_cache
import time

logger = logging.getLogger(__name__)
# logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

try:
    import IPython
except ImportError:
    pass




class OrionEntity(object):

    __slots__ = ['server', 'name', 'type']



    def __init__(self,server, id=None, type=None):
        self.name = id
        self.type = type
        self.server = server


    def checkpath(self,key):
        if not key.startswith("attrs/") :
            mkey = f"attrs/{key}"
        else :
            mkey = key
        return mkey

    def __repr__(self):
        return f"<{self.__module__}.{self.__class__.__name__} '{self.server.url}/v2/entities/{self.name}'>"

    def __getitem__(self, key):
        return self.server[self.name + "/" + self.checkpath(key)]

    def __setitem__(self,akey,value):
        key = self.name + "/" + self.checkpath(akey)
        if type(value) in (str,int,float,dict) and key.endswith("/value"):
            # update value of existing attribute
            self.server[key] = value
        elif type(value) in (dict,) and key.endswith("/attrs"):
            # update existing attribute(s) with dict
            self.server[key] = value
        else:
            raise ValueError(f"{self!r}: setting {akey} to {value!r} not possible")


    def update(self, key, value):
        current = self[key]
        if str(current) != str(value):
            self[key] = value
            return True
        else:
            return False


    def waitforchange(self, key, interval=2):
        oldval = self[key]
        current = str(oldval)
        time.sleep(interval)
        actual = self[key]
        while current == str(actual):
            time.sleep(interval)
            actual = self[key]
            logger.debug(f"{self} waiting for change")
        return dict(old=oldval, new=actual)


class Orion(object):

    __slots__ = ['url', 'path']


    def __init__(self,server, ServicePath=''):
        self.url = server
        self.path = ServicePath

    def __repr__(self):
        return f"<{self.__module__}.{self.__class__.__name__} url='{self.url}' ServicePath='{self.path}'>"


    def headers(self,d = {}):
        d.update({ "Fiware-ServicePath" : self.path })
        return d

    def __getitem__(self, key):
        if len(key)>0 and key[0] == "/":
            url = self.url + "/v2" + key
        else:
            url = self.url + "/v2/entities/" + key
        response = requests.get(url, headers=self.headers())
        return response.json()

    def __setitem__(self,key,value):
        if type(value) in (str,int,float,dict) and key.endswith("/value"):
            # update value of existing attribute
            result = requests.put(self.url+"/v2/entities/"+key,
                                headers=self.headers({'Content-type' : "text/plain" }),
                                data = json.dumps(value))
            logger.debug(f"PUT {value!r}")
        elif type(value) in (dict,) and key.endswith("/attrs"):
            # update existing attribute(s) with dict
            result = requests.patch(self.url+"/v2/entities/"+key,
                                headers=self.headers({'Content-type' : "application/json" }),
                                data = json.dumps(value))
            logger.debug(f"PUT {value!r}")
        else :
            raise ValueError(f"{self!r}: setting {key} to {value!r} not possible")
        result.raise_for_status()


    @lru_cache(200)
    def __getattr__(self, name):
        try:
            return OrionEntity(self,**self[f"{name}?attrs=id"])
        except Exception as e:
            raise AttributeError(f"{self!r} has no entity {name} - {e}")


    def query(self,qstr):
        r = self[qstr]
        if type(r) == list:
            for item in r:
                yield OrionEntity(self, id=item["id"], type=item.get("type",None))
        else:
            raise ValueError(f"{qstr}: {r!r}")


    def __setattr__(self, name, value):
        # IPython.embed()
        if name in self.__slots__:
           object.__setattr__(self, name, value)
        else:
            logger.debug(f"{self}: dynamic __setattr__ {name} {value}")
            try:
                if not hasattr(self,name):
                    # create entity
                    value["id"] = name
                    result = requests.post(self.url+"/v2/entities",
                                  headers=self.headers({"Content-Type" : "application/json" }),
                                  data=json.dumps(value))
                    logger.debug(f"POST data {value!r}")
                    result.raise_for_status()
                else:
                    # update entity
                    result = requests.put(self.url+"/v2/entities/"+name+"/attrs",
                                  headers=self.headers({"Content-Type" : "application/json" }),
                                  data=json.dumps(value))
                    logger.debug(f"PUT data {value!r}")
                    result.raise_for_status()
            except Exception as e:
                raise(e)




    def create_entity(self, name, spec):
        self.__setattr__(name, spec)
        return self.get_entity(name)

    update_entity = __setattr__

    get_entity = __getattr__


