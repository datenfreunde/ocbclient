import os
import sys
import logging

logger = logging.getLogger(__name__)

logging.basicConfig(level=logging.DEBUG, stream=sys.stderr)

from ocbclient  import Orion
from configuration import Config


def run(config):

    ocb = Orion(config.OUTPUT["broker"],
               ServicePath=config.OUTPUT["service"])
    import IPython
    IPython.embed(header="ocb is the broker")


if __name__ == '__main__':
    Config.update_from_dict(os.environ)
    run(Config)

