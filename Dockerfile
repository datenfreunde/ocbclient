
FROM mielune/alpine-python3-arm:edge

ENV LANG en_US.UTF-8

RUN mkdir /task

WORKDIR /task

COPY requirements.txt /task

RUN pip3 install --upgrade pip && \
    pip3 install -r requirements.txt


COPY . /task





