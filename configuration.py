class Config:

    INPUT = {
              "broker"  : "http://localhost:1026",
              "service" : "",
              "entity"  : "energy_production",
              "attribute" : ""
            }



    OUTPUT = {
              "broker"  : "http://localhost:1026",
              "service" : "",
              "entity"  : "charger",
              "attribute" : "state",
             }



    TEMPLATE = "Changed from {old!r} to {new!r}"


    @classmethod
    def update_from_dict(cls,dic):
        for (k,v) in dic.items():
            parts = k.split("_")
            if len(parts) == 2:
                a = parts[0].upper()
                n = parts[1].lower()
                if hasattr(cls, a.upper()) and n.lower() in getattr(cls, a.upper()):
                    getattr(cls,a.upper())[n.lower()]=v


