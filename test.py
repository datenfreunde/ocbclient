

# Connect to Context Broker

from ocbclient  import Orion

o = Orion("http://localhost:1026")

# Create Entity 'heaven' with two attributes

if not hasattr(o,"heaven"):
    o.heaven = dict(type="room", pressure=dict(value=105,type="Integer"), temperature=dict(value=25,type="Float"))


# Get temperature in heaven

print(o.heaven["attrs/temperature/value"])

# Set temperature in heaven

o.heaven["attrs/temperature/value"] = 22


# Get temperature in heaven

print(o.heaven["attrs/temperature/value"])

