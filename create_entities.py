
import os
import sys
import logging

logger = logging.getLogger(__name__)

logging.basicConfig(level=logging.INFO, stream=sys.stderr)

from ocbclient  import Orion
from configuration import Config


def create_entities(config):
    outb = Orion(config.OUTPUT["broker"],
                ServicePath=config.OUTPUT["service"])


    try:
        te = outb.get_entity(config.OUTPUT["entity"])
    except AttributeError:
        te = outb.create_entity(config.OUTPUT["entity"], dict(
                      type  = "switch",
                      state = dict(value=0,type="Integer")
                    ))
        logger.info(f'{te!r} created')
    else:
        logger.info(f'{te!r} exists')

    inb = Orion(config.INPUT["broker"],
                ServicePath=config.INPUT["service"])


    try:
        te = inb.get_entity(config.INPUT["entity"])
    except AttributeError:
        te = inb.create_entity(config.INPUT["entity"],dict(
                      type = 'grid_metric',
                      production = dict(type="Float", value=0),
                      predicted  = dict(type="Float", value=0)
                    ))
        logger.info(f'{te!r} created')
    else:
        logger.info(f'{te!r} exists')


if __name__ == "__main__":
    import os
    Config.update_from_dict(os.environ)
    create_entities(Config)
