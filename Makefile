SHELL := /bin/bash

IMAGE_NAME := oriontools

run-orion:
	docker stack deploy orion --compose-file=tests/orion-docker-compose.yml


requirements.txt requirements-dev.txt: Pipfile Pipfile.lock
	pipenv run pipenv_to_requirements -f


build: requirements.txt requirements-dev.txt
	docker build . -t $(IMAGE_NAME)

	


