import os
import sys
import logging

logger = logging.getLogger(__name__)


if "PYTHON_LOG_LEVEL" in os.environ:
    log_level = getattr(logging,os.environ["PYTHON_LOG_LEVEL"].upper())
else:
    log_level = logging.INFO

logging.basicConfig(level=log_level, stream=sys.stderr)

from ocbclient  import Orion
from configuration import Config


def run_relay(config):

    inp  = list(Orion(config.INPUT["broker"],
                 ServicePath=config.INPUT["service"])\
                 .query(f"?id={config.INPUT['entity']}"))[0]
    outp = list(Orion(config.OUTPUT["broker"],
                 ServicePath=config.OUTPUT["service"])\
                 .query(f"?id={config.OUTPUT['entity']}"))[0]


    logger.info(f"Waiting for change in {inp!r}")
    while True:
        try:
            changed = inp.waitforchange(config.INPUT['attribute'])
            if float(changed["new"]["production"]["value"]) > float(changed["new"]["predicted"]["value"]):
                switch_value = 1
            else :
                switch_value = 0
            logger.info(f"Change in {inp!r} detected. Writing {switch_value} to {outp!r}")
            outp[config.OUTPUT['attribute']+"/value"] = switch_value
        except Exception as e:
            logger.exception(e)


if __name__ == '__main__':
    Config.update_from_dict(os.environ)
    run_relay(Config)

