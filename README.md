# Orion Context Broker Tools

This Repo provides a set of dockerized tools for the [Orion Context Broker](https://fiware-orion.readthedocs.io/en/master/), which 
is a C++ implementation of the NGSIv2 REST API binding developed as a part of the FIWARE platform.


## How to use the Docker container as a Context Broker Relay

The demo app switches an imaginary battery charger based on changes in the electrical grid. 

The whole setup assumes you have a OCB running at `http://localhost:1026`. Please see "Orion Context Broker Test Instance" below for tips on how to do this.  If you want to brokers located elsewhere, please see below for configuration instructions.

You need `pipenv` to install, build and run.

Steps to run the demo:

1. Install the python environment

```
pipenv --python 3.6 install --dev 
```

1. Build the image.

```
make build
```

2. Ensure the necessary entities are created.

```
docker run --rm --network=host oriontools python3 create_entities.py
```

3. Run the relay
```
docker run --rm --network=host oriontools python3 relay.py
```


### How to configure the Docker container

Configuration uses environment variables, e.g. `INPUT_BROKER=http://orion.hosted.elsewhere.com`.

The following variables that can be used:

    - INPUT_BROKER
    - INPUT_SERVICE
    - INPUT_ENTITY
    - INPUT_ATTRIBUTE
    - OUTPUT_BROKER
    - OUTPUT_SERVICE
    - OUTPUT_ENTITY
    - OUTPUT_ATTRIBUTE

Also, PYTHON_DEBUG_LEVEL can be set to the usual values, e.g. DEBUG, LOG, INFO or ERROR.

The packaged demo application will listen to changes of the INPUT_ATTRIBUTE of the INPUT_ENTITY on the INPUT_BROKER and change the OUTPUT_ATTRIBUTE on the OUTPUT_BROKER accordingly.




## Orion Context Broker Test Instance
If you don't have an Orion instance available, you can use the `tests/orion-docker-compose.yml` to run one using docker stack.
Please refer to the `Makefile`.

This way, you'll get a running Orion Context Broker service at `http://localhost:1026`, consisting of the broker 
in one container and MongoDB used for storage in another. Data is not preserved across system restarts.




## ocbclient module Walkthorugh

This repo contains a `ocbclient` python 3.6 module that is used to connect to the Orion Context Broker.

Connect to Context Broker

```
from ocbclient import Orion

ocb = Orion("http://localhost:1026")
```

Entities are exposed as python attributes. To create a new entity named `heaven` with two sensor readings, pressure and temperature,
assign it a dict like so:

```
ocb.heaven = dict(type="room", 
                  pressure=dict(value=105,type="Integer"), 
                  temperature=dict(value=25,
                                   type="Float"
                                   metadata = { 
                                        "accuracy" : { "value": 0.1 }
                                    }
                                  )
                  )
```
See ["entity creation"](https://fiware-orion.readthedocs.io/en/master/user/walkthrough_apiv2/index.html#entity-creation) in the Orion manual.


Entity attributes are exposed via a dict-like interface. So to get the temperature in `heaven`:

```
print(ocb.heaven["temperature/value"])

25
```

Likewise, you can assign a value:

```

ocb.heaven["temperature/value"] = 22

print(ocb.heaven["temperature/value"])

22
```

To get a dict of all the attributes of an entity:

```
print(ocb.heaven[""])
```

To list all the entities that the broker knows of:

```
print(list(ocb.query("?")))
```


Query argument can contain specific conditions detailed in the [section "query entity" in the Orion Manual](https://fiware-orion.readthedocs.io/en/master/user/walkthrough_apiv2/#query-entity). 


To use the entity service path header feature described [in this section of the Orion manual](https://fiware-orion.readthedocs.io/en/master/user/service_path/index.html), initialize the client with a path: 

```
ocb_ottensen = Orion("http://localhost:1026", ServicePath="/Hamburg/Ottensen")
```



### Other implementations of Orion Context Broker libraries in Python

  * [Orion Python Client](https://github.com/openath/orion-python-client) - has many authentication options, but I couldn't make it work on Python 3.6.


## Ideas for improvement

  - Package ocbclient separately


## References 


(c) 2016-2018 Datenfreunde GmbH

This module is part of the [Smart Orchestra Project](smarto), co-financed by the
(Federal Ministery of Economics and Technology)[bmwi].


[smarto]: http://smartorchestra.de

[bmwi]: https://www.bmwi.de/

[git]: https://bitbucket.org/datenfreunde/ocbclient/src

